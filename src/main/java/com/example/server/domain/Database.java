package com.example.server.domain;

import java.util.ArrayList;
import java.util.List;

public class Database {
	private static List<Music> musicList;
	private static List<Video> videoList;
	static {
		musicList = new ArrayList<>();
		videoList = new ArrayList<>();
		musicList.add(new Music("S001.mp3","Artist_1", "2018", "Length_1",""));
		musicList.add(new Music("S002.mp3","Artist_2", "2016", "Length_2",""));
		musicList.add(new Music("S003.mp3","Artist_3", "2017", "Length_3",""));
		musicList.add(new Music("S004.mp3","Artist_4", "2012", "Length_4",""));
		musicList.add(new Music("S005.mp3","Artist_5", "2014", "Length_5",""));
		videoList.add(new Video("V001.mp3","Artist_1", "2018", "Length_1",""));
		videoList.add(new Video("V002.mp3","Artist_2", "2016", "Length_2",""));
		videoList.add(new Video("V003.mp3","Artist_3", "2017", "Length_3",""));
		videoList.add(new Video("V004.mp3","Artist_4", "2012", "Length_4",""));
		videoList.add(new Video("V005.mp3","Artist_5", "2014", "Length_5",""));
		}
	
	public static List<Music> getMusicList() {
		return musicList;
	}
	
	public static Music getAMusic(String name) {
		for(Music m : musicList)
			if(m.getName().equals(name))
				return m;
		return null;
	}

	public static Video getAVideo(String name) {
		for(Video v : videoList)
			if(v.getName().equals(name))
				return v;
		return null;
	}
}
